//
//  MainViewController.swift
//  TopPlayersApiApp
//
//  Created by DS on 26.10.2021.
//

import UIKit

// MARK: - MainViewController

class MainViewController: UIViewController {

    @IBOutlet weak var seasonTextField: UITextField!
    @IBOutlet weak var leagueTextField: UITextField!
    
    var playerArray: [PlayerModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func doneAction(_ sender: Any) {
        
        guard
            let seasonText = seasonTextField.text, let leagueText = leagueTextField.text, !seasonText.isEmpty, !leagueText.isEmpty else {
                
                let alert = UIAlertController(title: "Error", message: "Enter all fields!", preferredStyle: .alert)
                let cancelAction = UIAlertAction(title: "cancel", style: .cancel, handler: nil)
                alert.addAction(cancelAction)
                
                self.present(alert, animated: true, completion: nil)
                return
        }
        
        let playesVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PlayersViewController") as! PlayersViewController
        
        playesVC.season = seasonText
        playesVC.league = leagueText
        
        self.present(playesVC, animated: true, completion: nil)
    }
}
