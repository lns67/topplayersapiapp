//
//  PlayerModel.swift
//  TopPlayersApiApp
//
//  Created by DS on 26.10.2021.
//

import Foundation

// MARK: - PlayerModel

struct PlayerModel {
    var firstName: String
    var lastName: String
    var nationalyty: String
    var teamName: String
    var height: String
}
