//
//  PlayerTableViewCell.swift
//  TopPlayersApiApp
//
//  Created by DS on 26.10.2021.
//

import UIKit

// MARK: - PlayerTableViewCell

class PlayerTableViewCell: UITableViewCell {

    @IBOutlet weak var playerInfoLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func update(player: PlayerModel) {
        playerInfoLabel.text = "\(player.firstName) \(player.lastName) \n\(player.nationalyty) \(player.teamName) - \(player.height)"
    }
    
}
