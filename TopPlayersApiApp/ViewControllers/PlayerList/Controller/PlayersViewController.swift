//
//  PlayersViewController.swift
//  TopPlayersApiApp
//
//  Created by DS on 26.10.2021.
//

import UIKit

// MARK: - PlayersViewController

class PlayersViewController: UIViewController {

    @IBOutlet weak var playerListTableView: UITableView!
    @IBOutlet weak var backArrowImage: UIImageView!
    
    var season: String!
    var league: String!
    
    var playerArray: [PlayerModel] = []
    
    var playerCellId = "PlayerTableViewCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        playerListTableView.register(UINib(nibName: playerCellId, bundle: nil), forCellReuseIdentifier: playerCellId)
    }
        
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        getFootbalPlayers(league: league, season: season)
        playerListTableView.reloadData()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        playerListTableView.reloadData()
    }
    
    @IBAction func backAction(_ sender: Any) {
        playerArray.removeAll()
        self.dismiss(animated: true)
    }
}
