//
//  PlayersViewController+Extensions.swift
//  TopPlayersApiApp
//
//  Created by DS on 26.10.2021.
//

import Foundation
import UIKit

// MARK: - PlayersViewController + Extensions

// MARK: - UITableViewDelegate, UITableViewDataSource
extension PlayersViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return playerArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: playerCellId, for: indexPath) as! PlayerTableViewCell
        cell.update(player: playerArray[indexPath.row])
        return cell
    }
}

// MARK: - getFootbalPlayers
extension PlayersViewController {
    
    func getFootbalPlayers(league: String, season: String) {
        
        var urlComponent = URLComponents(string: "https://api-football-beta.p.rapidapi.com/players/topscorers")!
                     
        let parameterLeague = URLQueryItem(name: "league", value: league)
        let parameterSeason = URLQueryItem(name: "season", value: season)
        urlComponent.queryItems = [parameterLeague, parameterSeason]
        
        var request = URLRequest(url: urlComponent.url!)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = ["X-RapidAPI-Host" : "api-football-beta.p.rapidapi.com",
                                       "X-RapidAPI-Key" : "078330417bmsh94a5cd2f513d694p1616e8jsn59bf695086c9"]
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            if let data = data {
                do {
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                        
                        if let responseKey = json["response"] as? [[String : Any]] {
                            
                            for object in responseKey {
                                
                                if let team = object["player"] as? [String : Any] {
                                    
                                    let player = PlayerModel(firstName: team["firstname"] as! String, lastName: team["lastname"] as! String, nationalyty: team["nationality"] as! String, teamName: "", height: team["height"] as! String)
                                    
                                    self.playerArray.append(player)
                                }
                            }
                        } else if let message = json["message"] as? String {
                            
                            let alert = UIAlertController(title: "API Error", message: message, preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                            
                            alert.addAction(okAction)
                            
                            DispatchQueue.main.async {
                                
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }
                } catch let error as NSError {
                    
                    print(error.localizedDescription)
                }
            }
        }.resume()
    }
}
